import {userTypes} from './'

const initState = {
    currentUser: null,
}

export const userReducers = (state = initState, action) => {
    switch(action.type) {
        case userTypes.SET_USER:
            return { ...state, ...action.payload};
        case userTypes.SET_DEFAULT_USER:
            return initState;
        default:
            return state;
    }
}