export const userTypes = {
    SET_USER: 'SET_USER',
    SET_DEFAULT_USER: 'SET_DEFAULT_USER',
};