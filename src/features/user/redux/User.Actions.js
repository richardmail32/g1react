import { userTypes } from "./";
import { User } from '../../../services'

export const setUser = (payload) => ({
    type: userTypes.SET_USER,
    payload,
});

export const setDefaultUser = () => ({
    type: userTypes.SET_DEFAULT_USER,
});

export const getUser = () => async dispatch => {
    try {
        const { data } = await User.getUser();
        dispatch(
            setUser({
                currentUser: data
            })
        )
        console.log('getUser', data);
    } catch (error) {
        console.log('getUser', error);
    }
}