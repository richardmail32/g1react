import React from "react";
import { Dialog, Button } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";
import { connect } from "react-redux";
import { setDefaultAuthentication } from "../../authentication/redux";
import { setDefaultUser } from '../redux';

const DialogConfirm = ({ visible, setVisible, setDefaultAuthentication, setDefaultUser }) => {
    const navigation = useNavigation();
    const onLogout = () => {
        setDefaultAuthentication();
        setDefaultUser();
        navigation.reset({
            index: 0,
            routes: [
                {
                    name: 'Authentication.LoginScreen',
                },
            ],
        });
    }
    return (
        <Dialog visible={visible} onDismiss={() => setVisible(false)}>
            <Dialog.Title>Apakah kamu ingin keluar?</Dialog.Title>
            <Dialog.Actions>
                <Button onPress={() => setVisible(false)}>Tidak</Button>
                <Button onPress={onLogout}>Ya</Button>
            </Dialog.Actions>
        </Dialog>
    )
}


const mapDispatchtoProps = dispatch => ({
    setDefaultAuthentication: () => dispatch(setDefaultAuthentication()),
    setDefaultUser: () => dispatch(setDefaultUser()),
})


export default connect(null, mapDispatchtoProps)(DialogConfirm);