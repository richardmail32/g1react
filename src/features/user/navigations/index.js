import { UserScreen } from '../views';


export default {
    'User.UserScreen': {
        screen: UserScreen,
        options: {
            headerShown: false,
            contentStyle: {
                backgroundColor: '#ffffff'
            },
        },
    },
}