import React, { useCallback, useState } from 'react'
import { View, Text, StyleSheet, ScrollView, RefreshControl } from 'react-native'
import { ActivityIndicator, Avatar, Button, Title, List } from 'react-native-paper';
import Images from '../../../../assets';
import { useFocusEffect } from '@react-navigation/native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect'
import { getUser, selectCurrentUser } from '../../redux';
import { useTheme } from 'react-native-paper'
import DialogConfirm from '../../components/DialogConfirm';

function UserScreen({ currentUser, getUser }) {
    const [visible, setVisible] = useState(false);
    const theme = useTheme()
    console.log('currentUser:', currentUser);
    useFocusEffect(
        useCallback(() => {
            getUser();
            return () => setVisible(false);

        }, [],
        ))

    const refreshComponent = (
        <RefreshControl
            colors={[theme.colors.primary]}
            refreshing={false}
            onRefresh={getUser}
        />
    )


    if (!currentUser) {
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
                refreshControl={refreshComponent}>
                <ActivityIndicator size="small" />
            </ScrollView>
        )
    }

    return (
        <View style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ flexGrow: 1 }}
                refreshControl={refreshComponent}>
                <View style={styles.header}>
                    <Avatar.Image size={48} source={Images.logo} />
                    <View style={styles.subHeader}>
                        <Title>{currentUser.nama_depan} {currentUser.last_name}</Title>
                        <Text>{currentUser.msisdn}</Text>
                    </View>
                </View>
                <View style={styles.header}>
                    <List.Section>
                        <List.Subheader>Some title</List.Subheader>
                        <List.Item title="First Item" left={() => <List.Icon icon="folder" />} />
                        <List.Item
                            title="Second Item"
                            left={() => <List.Icon color="#000" icon="folder" />}
                        />
                    </List.Section>
                </View>
                <Button
                    mode='outlined'
                    style={styles.button}
                    icon="logout"
                    onPress={() => setVisible(true)}>
                    <Text style={{ color: '#b71c1c', textTransform: 'capitalize' }}>
                        Keluar dari akun
                    </Text>
                </Button>
            </ScrollView>
            <DialogConfirm visible={visible} setVisible={setVisible} />
        </View>
    )
}

// const mapStateToProps = state => ({
//     currentUser: state.user.currentUser
// })

const mapStateToProps = createStructuredSelector({
    currentUser: selectCurrentUser
})

const mapDispatchToProps = dispatch => ({
    getUser: () => dispatch(getUser())
})

export default connect(mapStateToProps, mapDispatchToProps)(UserScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        padding: 15,
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        elevation: 5,
        shadowColor: 'red',
        marginBottom: 10
    },
    subHeader: {
        marginLeft: 15,
        flex: 1
    },
    button: {
        // backgroundColor: '#b71c1c',
        justifyContent: 'center',
        marginHorizontal: 15,
        marginTop: 20,
        height: 44,
        borderColor: 'red',
        borderWidth: 1,
        borderRadius: 10
    },
    heading: {
        fontSize: 18,
        fontWeight: '600',
        marginBottom: 13,
    },
});