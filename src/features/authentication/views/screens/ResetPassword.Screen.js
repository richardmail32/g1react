import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react'
import { View, Text, StyleSheet, Alert } from 'react-native'
import { Button, Caption, Surface, TextInput, useTheme } from 'react-native-paper';
import { Authentication } from '../../../../services';

function ResetPasswordScreen() {
    const navigation = useNavigation();
    const theme = useTheme();
    const [phoneNumber, setPhoneNumber] = useState('');
    const [phoneNumberError, setPhoneNumberError] = useState(false);
    const [loading, setLoading] = useState(false);

    const onSubmit = () => {
        const pattern = '^(^08)(\\d{3,4}-?){2}\\d{3,4}$';
        const reg = new RegExp(pattern, 'g');

        if (!phoneNumber.length || !reg.test(phoneNumber)) {
            setPhoneNumberError(true);
            return;
        } else {
            reset();
        }
    }

    const reset = async () => {
        setLoading(true);
        try {
            const reqBody = {
                msisdn: phoneNumber
            };

            let formData = new FormData();

            for (const key in reqBody) {
                formData.append(key, reqBody[key]);
            }
            await Authentication.resetPassword(formData);
            Alert.alert('Reset Password', 'Berhasil', [
                { text: 'OK', onPress: () => { navigation.navigate('LoginScreen') } }
            ]);

        } catch (error) {
            console.log(error);
        } finally {
            setLoading(false);
        }
    }


    return (
        <View style={styles.screen}>
            <Surface style={styles.surface}>
                <Text style={styles.text}>
                    Masukan No. Handphone Anda yang terdaftar {'\n'} di Boom Motorist.
                </Text>
                <TextInput
                    label="No. Handphone"
                    placeholder="Masukan no.hp yang valid"
                    mode='outlined'
                    onChangeText={text => {
                        setPhoneNumber(text);
                        setPhoneNumberError(false);
                    }}
                    error={phoneNumberError}
                    left={<TextInput.Icon name="cellphone-android" />}
                    keyboardType="phone-pad"
                    disabled={loading}
                />
                {phoneNumberError && (
                    <Caption style={{ color: theme.colors.error }}>
                        No. Hp harus valid!
                    </Caption>
                )}
                <Button
                    mode="contained"
                    onPress={onSubmit}
                    loading={loading}
                    disabled={loading}
                    style={styles.button}>
                    <Text style={{ color: '#ffffff', textTransform: 'capitalize' }}>
                        Reset
                    </Text>
                </Button>
            </Surface>
        </View>
    )
}

export default ResetPasswordScreen;

const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    text: {
        textAlign: 'center',
        marginBottom: 20,
        fontWeight: 'bold',
        fontSize: 16
    },
    surface: {
        margin: 8,
        padding: 10,
        elevation: 7,
        borderRadius: 5
    },
    button: {
        backgroundColor: '#b71c1c',
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: 10,
        height: 40,
    }
});