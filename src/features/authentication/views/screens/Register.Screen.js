import { Picker } from '@react-native-picker/picker';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import React, { useCallback, useState, useEffect } from 'react'
import { View, Text, ScrollView, StyleSheet, Alert, RefreshControl } from 'react-native'
import { TextInput, Button, useTheme } from 'react-native-paper';
import { connect } from 'react-redux';
import { Authentication } from '../../../../services';
import { getStockist, selectCurrentStockist } from '../../redux';
import { createStructuredSelector } from 'reselect'
import { Toast } from '../../../../configs';

function RegisterScreen({ currentStockist, getStockist }) {
    const [visible, setVisible] = useState(false);
    const navigation = useNavigation();
    const theme = useTheme();

    const [showPassword, setShowPassword] = useState(true);
    const [passwordError, setPasswordError] = useState(false);
    const [firstNameError, setFirstNameError] = useState(false);
    const [lastNameError, setLastNameError] = useState(false);
    const [storeError, setStoreError] = useState(false);
    const [msisdnError, setMsisdnError] = useState(false);

    const [nama_depan, setFirstName] = useState('');
    const [nama_belakang, setLastName] = useState('');
    const [msisdn, setMsisdn] = useState('');
    const [nama_usaha, setStore] = useState('');
    const [nama_pasar, setSelectedStockist] = useState('');
    const [password, setPassword] = useState('');
    const [konfirmasi_password, setConfirmPassword] = useState('');
    const [latitude, setLatitude] = useState('');
    const [longitude, setLongitude] = useState('');

    console.log('currentStockist:', currentStockist);
    useFocusEffect(
        useCallback(() => {
            getStockist();
            return () => setVisible(false);
        }, [],
        ))

    useEffect(() => {
        setFirstNameError(false);
        setLastNameError(false);
        setPasswordError(false);
        setStoreError(false);
        setMsisdnError(false)
    }, [nama_depan, nama_belakang, msisdn, nama_usaha, nama_pasar, password, latitude, longitude]);



    const onRegister = async () => {
        try {
            const reqBody = {
                nama_depan,
                nama_belakang,
                msisdn,
                nama_usaha,
                nama_pasar,
                password,
                konfirmasi_password,
                latitude: '-6.2593825036990225',
                longitude: '106.6378067479457',
            };

            let formData = new FormData();

            for (const key in reqBody) {
                formData.append(key, reqBody[key]);
            }

            await Authentication.register(formData);
            console.log(formData);
            // navigation.goBack();
            navigation.reset({
                index: 0,
                routes: [
                    {
                        name: 'Authentication.LoginScreen',
                    },
                ],
            });
        } catch (error) {
            if (error.response && error.response.status === 400) {
                Toast('error')
            }
            console.log(error);
        }
    }

    const refreshComponent = (
        <RefreshControl
            colors={[theme.colors.primary]}
            refreshing={false}
            onRefresh={getStockist}
        />
    )

    return (
        <ScrollView contentContainerStyle={{
            flexGrow: 1,
        }} scrollIndicatorInsets={false} refreshControl={refreshComponent}>
            <View style={{
                paddingHorizontal: 20,
                paddingTop: 20
            }}>
                <TextInput
                    label="Nama Depan"
                    placeholder="Masukan Nama Depan Anda"
                    value={nama_depan}
                    onChangeText={text => setFirstName(text)}
                    left={<TextInput.Icon name="account-circle" />}
                    mode='outlined'
                    style={styles.text}
                />
                {firstNameError && (
                    <Caption style={{ color: theme.colors.error }}>
                        Nama Depan harus diisi!
                    </Caption>
                )}


                <TextInput
                    label="Nama Belakang"
                    placeholder="Masukan Nama Belakang Anda"
                    value={nama_belakang}
                    onChangeText={text => setLastName(text)}
                    left={<TextInput.Icon name="account" />}
                    mode='outlined'
                    style={styles.text}
                />
                {lastNameError && (
                    <Caption style={{ color: theme.colors.error }}>
                        Nama Belakang harus diisi!
                    </Caption>
                )}

                <View style={{
                    borderWidth: 0.5,
                    borderRadius: 5,
                    margin: 10
                }}>
                    <Picker
                        selectedValue={nama_pasar}
                        style={{
                            height: 50,
                            width: '100%'
                        }}
                        itemStyle='pilih'
                        onValueChange={(itemValue, itemIndex) => setSelectedStockist(itemValue)}
                    >
                        {currentStockist?.map((data) => (
                            <Picker.Item label={data.stockist} value={data.stockist} />
                        ))}
                    </Picker>
                </View>


                <TextInput
                    label="Nama Usaha"
                    placeholder="Masukan Nama Usaha Anda"
                    value={nama_usaha}
                    onChangeText={text => setStore(text)}
                    left={<TextInput.Icon name="email" />}
                    mode='outlined'
                    style={styles.text}
                // keyboardType='email-address'
                />
                {storeError && (
                    <Caption style={{ color: theme.colors.error }}>
                        Nama Usaha harus diisi!
                    </Caption>
                )}

                <TextInput
                    label="No. Handphone"
                    placeholder="Masukan no.hp yang valid"
                    value={msisdn}
                    onChangeText={text => setMsisdn(text)}
                    left={<TextInput.Icon name="cellphone-android" />}
                    mode='outlined'
                    style={styles.text}
                    keyboardType='phone-pad'
                />
                {msisdnError && (
                    <Caption style={{ color: theme.colors.error }}>
                        No. Hp harus valid!
                    </Caption>
                )}

                <TextInput
                    label="Kata sandi"
                    placeholder="Masukan Kata sandi"
                    value={password}
                    onChangeText={text => setPassword(text)}
                    left={<TextInput.Icon name="lock-outline" />}
                    mode='outlined'
                    style={styles.text}
                    secureTextEntry={showPassword}
                    right={
                        <TextInput.Icon
                            name={showPassword ? 'eye-off' : 'eye'}
                            onPress={() => setShowPassword(!showPassword)}
                        />
                    }
                />
                {passwordError && (
                    <Caption style={{ color: theme.colors.error }}>
                        Kata sandi harus diisi!
                    </Caption>
                )}
                <TextInput
                    label="Konfirmasi Kata sandi"
                    placeholder="Masukan kembali Kata sandi Anda"
                    value={konfirmasi_password}
                    onChangeText={text => setConfirmPassword(text)}
                    left={<TextInput.Icon name="lock-outline" />}
                    mode='outlined'
                    style={styles.text}
                    secureTextEntry={showPassword}
                    right={
                        <TextInput.Icon
                            name={showPassword ? 'eye-off' : 'eye'}
                            onPress={() => setShowPassword(!showPassword)}
                        />
                    }
                />
                {passwordError && (
                    <Caption style={{ color: theme.colors.error }}>
                        Kata sandi harus diisi!
                    </Caption>
                )}

                <Button
                    mode="contained"
                    onPress={onRegister}
                    style={styles.button}
                >
                    <View style={{ width: 16, height: 1 }} />
                    <Text style={{ color: '#ffffff', textTransform: 'capitalize' }}>
                        Daftar
                    </Text>
                </Button>
            </View>


        </ScrollView>
    )
}

const mapStateToProps = createStructuredSelector({
    currentStockist: selectCurrentStockist,
})

const mapDispatchToProps = dispatch => ({
    getStockist: () => dispatch(getStockist()),
})


export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);


const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    login_logo: {
        height: 200,
        width: 280
    },
    button: {
        backgroundColor: '#b71c1c',
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: -10,
        height: 40,
    },
    text: {
        marginHorizontal: 10,
    }
});