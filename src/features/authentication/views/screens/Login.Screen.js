import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Alert, Image, TouchableOpacity, ScrollView } from 'react-native';
import { useTheme } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import { TextInput, Caption, Button, Text, Card } from 'react-native-paper';
import { Authentication } from '../../../../services';
import { Toast } from '../../../../configs'
import Images from '../../../../assets';
import { setAuthentication } from '../../redux';
import { connect } from 'react-redux';

function LoginScreen({ setAuthentication }) {
    const navigation = useNavigation();
    const theme = useTheme();

    const [username, setusername] = useState('');
    const [usernameError, setusernameError] = useState(false);

    const [password, setPassword] = useState('');
    const [passwordError, setPasswordError] = useState(false);
    const [showPassword, setShowPassword] = useState(true);

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setusernameError(false);
        setPasswordError(false);
    }, [username, password]);

    const onSubmitHandler = () => {
        if (!username.length) {
            setusernameError(true);
        }

        if (!password.length) {
            setPasswordError(true);
        }

        if (!username.length || !password.length) {
            return;
        } else {
            onLogin();
        }
    };

    const onLogin = async () => {
        setLoading(true);
        try {
            const reqBody = {
                username,
                password,
            };

            let formData = new FormData();

            for (const key in reqBody) {
                formData.append(key, reqBody[key]);
            }
            const { data } = await Authentication.login(formData);
            console.log(formData);
            setAuthentication({ userToken: data.api_token })
            navigation.reset({
                index: 0,
                routes: [
                    {
                        name: 'MainMenuScreen',
                    },
                ],
            });


        } catch (error) {
            // if (error.response && error.response.status === 400) {
            //     Toast('error',
            //         'Mohon Maaf Akun tidak Sesuai!',
            //         'Silahkan periksa kembali No. Hp dan kata sandi')
            // }
            // console.log(error);

        } finally {
            setLoading(false);
        }
    };

    return (
        <ScrollView contentContainerStyle={{
            flexGrow: 1,
            justifyContent: 'center'
        }} scrollIndicatorInsets={false} style={styles.container}>
            <View style={styles.container_img}>
                <Image
                    source={Images.logo}
                    resizeMode="contain"
                    style={{ height: 100 }}
                />
            </View>
            <Card style={styles.card} mode='outlined' >
                <View style={{ width: '100%', paddingHorizontal: 30 }}>
                    <TextInput
                        label="No. Handphone"
                        placeholder="Masukan no.hp yang valid"
                        value={username}
                        onChangeText={text => setusername(text)}
                        error={usernameError}
                        left={<TextInput.Icon name="cellphone-android" />}
                        keyboardType="phone-pad"
                        disabled={loading}
                        mode='outlined'
                    />
                    {usernameError && (
                        <Caption style={{ color: theme.colors.error }}>
                            No. Hp harus valid!
                        </Caption>
                    )}


                    <TextInput
                        label="Kata Sandi"
                        placeholder="Masukan kata sandi"
                        value={password}
                        onChangeText={text => setPassword(text)}
                        error={passwordError}
                        left={<TextInput.Icon name="lock-outline" />}
                        mode='outlined'
                        right={
                            <TextInput.Icon
                                name={showPassword ? 'eye-off' : 'eye'}
                                onPress={() => setShowPassword(!showPassword)}
                            />
                        }
                        style={{ marginTop: 15 }}
                        secureTextEntry={showPassword}
                        disabled={loading}
                    />
                    {passwordError && (
                        <Caption style={{ color: theme.colors.error }}>
                            Kata sandi harus diisi!
                        </Caption>
                    )}


                    <Text onPress={() => navigation.navigate('Authentication.ResetPasswordScreen')} style={{ marginTop: 10, color: 'red' }}>
                        Lupa Kata Sandi?
                    </Text>


                    <Button
                        mode="contained"
                        onPress={onSubmitHandler}
                        loading={loading}
                        disabled={loading}
                        style={styles.button}>
                        {/* Masuk */}
                        <View style={{ width: 16, height: 1 }} />
                        <Text style={{ color: '#ffffff', textTransform: 'capitalize' }}>
                            Masuk
                        </Text>
                    </Button>
                </View>
            </Card>



            <View style={{ marginTop: 80, justifyContent: 'center' }}>
                <Text style={{ textAlign: 'center', fontWeight: '700' }}>
                    Belum Punya Akun?
                </Text>
                <TouchableOpacity onPress={() => navigation.navigate('Authentication.RegisterScreen')} >
                    <Text style={{
                        color: 'red',
                        fontWeight: 'bold',
                        textAlign: 'center'
                    }}>Daftar Sekarang</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
}

const mapDispatchtoProps = (dispatch) => ({
    setAuthentication: payload => dispatch(setAuthentication(payload)),
})

export default connect(null, mapDispatchtoProps)(LoginScreen);

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fefe'

    },
    container_img: {
        alignItems: 'center',
        marginBottom: 10
    },
    card: {
        paddingVertical: 40,
        margin: 20,
        borderRadius: 20
    },

    login_logo: {
        height: 200,
        width: 280
    },
    button: {
        backgroundColor: '#b71c1c',
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: -10,
        height: 40,
        borderRadius: 10
    }
});
