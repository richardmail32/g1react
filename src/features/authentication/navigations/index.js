// import { LoginScreen, RegisterScreen, ResetPasswordScreen } from '../views';


// export default {
//     'Authentication.LoginScreen': {
//         screen: LoginScreen,
//         options: {
//             headerShown: false,
//             contentStyle: {
//                 backgroundColor: '#ffffff'
//             },
//         },
//     },

//     'Authentication.RegisterScreen': {
//         screen: RegisterScreen,
//         options: {
//             headerShown: false,
//             title: 'Register',
//             contentStyle: {
//                 backgroundColor: '#ffffff'
//             },
//         },
//     },

//     'Authentication.ResetPasswordScreen': {
//         screen: ResetPasswordScreen,
//         options: {
//             headerShown: false,
//             title: 'Reset Kata Sandi',
//             contentStyle: {
//                 backgroundColor: '#ffffff'
//             },
//         }
//     }
// }

import React from 'react';
import { LoginScreen, RegisterScreen, ResetPasswordScreen } from '../views'

export default {
    'Authentication.LoginScreen': {
        screen: LoginScreen,
        options: {
            headerShown: false,
            contentStyle: {
                backgroundColor: '#FFFFFF',
            },
        },
    },
    'Authentication.RegisterScreen': {
        screen: RegisterScreen,
        options: {
            title: 'Register'
        },
    },
    'Authentication.ResetPasswordScreen': {
        screen: ResetPasswordScreen,
        options: {
            title: 'Reset Kata Sandi'
        },
    },

};