/* eslint-disable prettier/prettier */
import { authenticationTypes } from './'

const initState = {
    userToken: null,
    currentStockist: null,
};

export const authenticationReducers = (state = initState, action) => {
    switch (action.type) {
        case authenticationTypes.SET_AUTHENTICATION:
            return { ...state, ...action.payload };
        case authenticationTypes.SET_DEFAULT_AUTHENTICATION:
            return initState;
        default:
            return state;
    }
};
