/* eslint-disable prettier/prettier */
import { createSelector } from 'reselect'

const selectStockist = (state) => state.authentication;

export const selectCurrentStockist = createSelector(
    [selectStockist],
    authentication => authentication.currentStockist
);