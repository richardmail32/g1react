/* eslint-disable prettier/prettier */
import { Authentication } from "../../../services";
import { authenticationTypes } from "./";

export const setAuthentication = (payload) => ({
    type: authenticationTypes.SET_AUTHENTICATION,
    payload,
});

export const setDefaultAuthentication = () => ({
    type: authenticationTypes.SET_DEFAULT_AUTHENTICATION,
});

export const setStockist = (payload) => ({
  type: authenticationTypes.SET_AUTHENTICATION,
    payload
});

export const setDefaultStockist = () => ({
    type: authenticationTypes.SET_DEFAULT_AUTHENTICATION,
});

export const getStockist = () => async dispatch => {
    try {
        const { data } = await Authentication.getStockist();
        dispatch(
            setStockist({
                currentStockist: data
            })
        )
        console.log('currentStockist', data);
    } catch (error) {
        console.log('setStockist', error);
    }
}