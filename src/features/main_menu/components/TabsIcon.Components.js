import React from "react";
import { useTheme } from "react-native-paper";
import Icons from "react-native-vector-icons/MaterialCommunityIcons"


const TabsIcon = ({ focused, name, size }) => {

    const theme = useTheme();
    return (
        <Icons
            name={name}
            size={size}
            color={focused ? theme.colors.primary : theme.colors.disabled}
            style={{ marginTop: 3 }}
        />
    );
};

export default TabsIcon;