import React from "react";
import { Text, useTheme } from "react-native-paper";


const TabsLabel = ({ focused, title }) => {
    const theme = useTheme();
    return (
        <Text
            style={{
                color: focused ? theme.colors.primary : theme.colors.disabled,
                textAlign: 'center',
                fontSize: 12,
                fontWeight: focused ? '700' : 'normal',
                marginBottom: 5,
            }}>
            {title}
        </Text>
    )


};

export default TabsLabel;