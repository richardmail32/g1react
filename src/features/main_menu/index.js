import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import { DashboardScreen } from '../dashboard/views';
import { DiscountScreen } from '../discount/views';
import { ScanScreen } from '../scan/views';
import { TransactionScreen } from '../transaction/views'
import { UserScreen } from '../user/views';
import { TabsIcon } from './components';
import { TabsLabel } from './components';
import { Avatar } from 'react-native-paper';
import { Image, TouchableOpacity } from 'react-native';
import images from '../../assets';
import { useNavigation } from '@react-navigation/native';


const MainMenuStack = createBottomTabNavigator();
const MainMenuScreen = () => {
    const navigation = useNavigation();
    return (
        <MainMenuStack.Navigator>
            <MainMenuStack.Screen
                name="Dashboard.DashboardScreen"
                component={DashboardScreen}
                options={{
                    title: 'Beranda',
                    headerRight: () => (
                        <TouchableOpacity onPress={() => navigation.navigate('Cart.CartScreen')}>
                            <Image
                                style={{
                                    width: 35,
                                    height: 35,
                                    margin: 20
                                }}
                                source={images.cart}
                            />
                        </TouchableOpacity>
                    ),
                    tabBarLabel: ({ focused }) => (
                        <TabsLabel focused={focused} title="Beranda" />
                    ),
                    tabBarIcon: ({ focused, size }) => (
                        <TabsIcon focused={focused} name={focused ? 'home' : 'home-outline'} size={size} />
                    ),
                    tabBarStyle: {
                        height: 55
                    }
                }}
            />

            <MainMenuStack.Screen
                name="Discount.DiscountScreen"
                component={DiscountScreen}
                options={{
                    title: 'Promo',
                    tabBarLabel: ({ focused }) => (
                        <TabsLabel focused={focused} title="Promo" />
                    ),
                    tabBarIcon: ({ focused, size }) => (
                        <TabsIcon focused={focused} name={focused ? 'ticket-percent' : 'ticket-percent-outline'} size={size} />
                    ),
                    tabBarStyle: {
                        height: 55
                    }
                }}
            />
            <MainMenuStack.Screen
                name="Scan.ScanScreen"
                component={ScanScreen}
                options={{
                    title: 'Scan',
                    tabBarLabel: () => null,
                    tabBarIcon: ({ focused, size }) => (
                        <Avatar.Icon
                            size={60}
                            icon={focused ? 'qrcode-scan' : 'qrcode'}
                            style={{ top: -20 }} />
                    ),
                    tabBarStyle: {
                        height: 55
                    }
                }}
            />

            <MainMenuStack.Screen
                name="Transaction.TransactionScreen"
                component={TransactionScreen}
                options={{
                    title: 'Transaksi',
                    tabBarLabel: ({ focused }) => (
                        <TabsLabel focused={focused} title="Transaksi" />
                    ),
                    tabBarIcon: ({ focused, size }) => (
                        <TabsIcon focused={focused} name={focused ? 'clipboard-text' : 'clipboard-text-outline'} size={size} />
                    ),
                    tabBarStyle: {
                        height: 55
                    }
                }}
            />
            <MainMenuStack.Screen
                name="User.UserScreen"
                component={UserScreen}
                options={{
                    title: 'Profil',
                    headerStyle: {
                        elevation: 0,
                    },
                    tabBarLabel: ({ focused }) => (
                        <TabsLabel focused={focused} title="Profil" />
                    ),
                    tabBarIcon: ({ focused, size }) => (
                        <TabsIcon focused={focused} name={focused ? 'account-circle' : 'account-circle-outline'} size={size} />
                    ),
                    tabBarStyle: {
                        height: 55
                    }
                }}
            />
        </MainMenuStack.Navigator>
    )
}

export default {
    MainMenuScreen: {
        screen: MainMenuScreen,
        options: {
            headerShown: false,
        },
    },
};
