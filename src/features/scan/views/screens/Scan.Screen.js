
import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

function ScanScreen () {
    return (
        <View style={styles.screen}>
            <Text>Scan Screen</Text>
        </View>
    );
}

export default ScanScreen;

const styles = StyleSheet.create({
    screen:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})