import { TransactionScreen, TransactionDetailScreen } from '../views';


export default {
    'Transaction.TransactionScreen': {
        screen: TransactionScreen,
        options: {
            headerShown: false,
            contentStyle: {
                backgroundColor: '#ffffff'
            },
        },
    },

    'Transaction.TransactionDetailScreen': {
        screen: TransactionDetailScreen,
        options: {
            title: 'Transaksi Detail',
            contentStyle: {
                backgroundColor: '#ffffff'
            },
        },
    },
}