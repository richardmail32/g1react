import { transactionTypes } from ".";
import { Transaction } from '../../../services'

export const setTransaction = (payload) => ({
    type: transactionTypes.SET_TRANSACTION,
    payload,
});

export const setDefaultTransaction = () => ({
    type: transactionTypes.SET_DEFAULT_TRANSACTION,
});

export const setTransactionDetail = (payload) => ({
    type: transactionTypes.SET_TRANSACTION,
    payload,
});

export const setDefaultTransactionDetail = () => ({
    type: transactionTypes.SET_DEFAULT_TRANSACTION,
});

export const getPo = () => async dispatch => {
    try {
        const { data } = await Transaction.getPo();
        dispatch(
            setTransaction({
                currentTransaction: data
            })
        )
        // console.log('setTransaction', data);
    } catch (error) {
        // console.log('setTransaction', error);
    }
}

export const getPoDetail = (id) => async dispatch => {
    console.log(id);
    try {
        const { data } = await Transaction.getPoDetail(id);
        dispatch(
            setTransactionDetail({
                currentTransactionDetail: data
            })
        )
        console.log('currentTransactionDetail', data);
    } catch (error) {
        console.log('setTransactionDetail', error);
    }
}