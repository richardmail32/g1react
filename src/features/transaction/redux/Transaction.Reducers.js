import { transactionTypes } from './'

const initState = {
    currentTransaction: null,
    currentTransactionDetail: null,
}

export const transactionReducers = (state = initState, action) => {
    switch (action.type) {
        case transactionTypes.SET_TRANSACTION:
            return { ...state, ...action.payload };
        case transactionTypes.SET_DEFAULT_TRANSACTION:
            return initState;
        default:
            return state;
    }
}