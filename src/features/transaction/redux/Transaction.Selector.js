import { createSelector } from 'reselect'

const selectTransaction = (state) => state.transaction;
const selectTransactionDetail = (state) => state.transaction;

export const selectCurrentTransaction = createSelector(
    [selectTransaction],
    transaction => transaction.currentTransaction
);


export const selectCurrentTransactionDetail = createSelector(
    [selectTransactionDetail],
    transaction => transaction.currentTransactionDetail
);