import { Image, SafeAreaView, ScrollView, StyleSheet, View } from 'react-native'
import React, { useCallback, useState } from 'react'
import { getPoDetail, selectCurrentTransactionDetail } from '../../redux';
import { connect } from 'react-redux';
import { useTheme, Title, Text, Card } from 'react-native-paper';
import { useFocusEffect } from '@react-navigation/native';
import { createStructuredSelector } from 'reselect'


function TransactionDetailScreens({ currentTransactionDetail, getPoDetail, route }) {
    const [visible, setVisible] = useState(false);
    const { po } = route.params;
    console.log('currentTransactionDetail:', currentTransactionDetail);
    useFocusEffect(
        useCallback(() => {
            getPoDetail(po);
            return () => setVisible(false);
        }, [po],
        ))

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView>
                <View>
                    <Text style={{ padding: 15, fontSize: 16, fontWeight: '700' }}>
                        {currentTransactionDetail?.po.status_po}
                    </Text>
                </View>
                <View style={styles.text}>
                    <Text style={{ fontSize: 14 }}>
                        No. Invoice
                    </Text>
                    <Text style={{ fontSize: 14, fontWeight: '700' }}>
                        {currentTransactionDetail?.po.kode_po}
                    </Text>
                </View>
                <View style={styles.text}>
                    <Text style={{ fontSize: 14 }}>
                        Tanggal
                    </Text>
                    <Text style={{ fontSize: 14, fontWeight: '700' }}>
                        {currentTransactionDetail?.po.tanggal}
                    </Text>
                </View>
                {currentTransactionDetail?.barang.map((detail, index) => (
                    <Card style={{ marginTop: 15 }}>
                        <View style={styles.header}>
                            <Image
                                key={index}
                                style={{
                                    height: 60,
                                    width: 60,
                                }}
                                source={{ uri: detail.foto_produk }}
                            />
                            <View style={styles.subHeader}>
                                <Title style={{ fontSize: 16 }}>
                                    {detail.nama_produk}
                                </Title>
                                <Text>
                                    {detail.jumlah}x Rp {Number.parseInt(detail.total).toLocaleString('id-ID')}
                                </Text>
                            </View>
                        </View>
                    </Card>
                ))}
                {currentTransactionDetail?.promo.map((promo, index) => (
                    <View key={index} style={styles.promo}>
                        {promo.tipe === 'product' && <View>
                            <Text style={{ fontSize: 14, fontWeight: '700' }}>
                                {promo.nama}
                            </Text>
                            <Text style={{ fontSize: 14, fontWeight: '700', color: 'red' }}>
                                Anda Mendapatkan hadiah
                            </Text>
                            {promo?.produk.map(prod => (
                                <View key={index}>
                                    <Text style={{ fontSize: 14, fontWeight: '700', color: 'red' }}>
                                        {prod.total_produk} {prod.nama}
                                    </Text>
                                </View>
                            ))}
                        </View>}
                        {promo.tipe !== 'product' && <View>
                            <Text style={{ fontSize: 14, fontWeight: '700' }}>
                                {promo.nama}
                            </Text>
                            <Text style={{ fontSize: 14, fontWeight: '700', color: 'red' }}>
                                Diskon Rp {promo.total_nominal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                            </Text>

                        </View>}
                    </View>
                ))}

                <View>
                    <Title style={{ padding: 15, fontSize: 16, fontWeight: '700' }}>
                        Detail Pembayaran
                    </Title>
                    <View style={[styles.text, { marginBottom: 20 }]}>
                        <Text style={{ fontSize: 14 }}>
                            Metode Pembayaran
                        </Text>
                        <Text style={{ fontSize: 14, fontWeight: '700' }}>
                            {currentTransactionDetail?.po.nama_tipe_pembayaran}
                        </Text>
                    </View>
                    <View style={styles.text}>
                        <Text style={{ fontSize: 14 }}>
                            Total Harga
                        </Text>
                        <Text style={{ fontSize: 14, fontWeight: '700' }}>
                            Rp {currentTransactionDetail?.po.subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                        </Text>
                    </View>
                    <View style={styles.text}>
                        <Text style={{ fontSize: 14 }}>
                            Biaya Layanan
                        </Text>
                        <Text style={{ fontSize: 14, fontWeight: '700' }}>
                            Rp {currentTransactionDetail?.po.biaya_layanan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                        </Text>
                    </View>
                    <View style={styles.text}>
                        <Text style={{ fontSize: 14 }}>
                            Voucher
                        </Text>
                        <Text style={{ fontSize: 14, fontWeight: '700', color: 'red' }}>
                            -Rp {currentTransactionDetail?.po.biaya_potongan_voucher.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                        </Text>
                    </View>
                    <View style={styles.text}>
                        <Text style={{ fontSize: 14 }}>
                            Promo
                        </Text>
                        <Text style={{ fontSize: 14, fontWeight: '700', color: 'red' }}>
                            -Rp {currentTransactionDetail?.po.biaya_potongan_promo.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                        </Text>
                    </View>
                    <View style={[styles.text, { marginVertical: 20 }]}>
                        <Text style={{ fontSize: 16, fontWeight: '700' }}>
                            Total Belanja
                        </Text>
                        <Text style={{ fontSize: 16, fontWeight: '700' }}>
                            Rp {currentTransactionDetail?.po.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                        </Text>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>


    )


}

const mapStateToProps = createStructuredSelector({
    currentTransactionDetail: selectCurrentTransactionDetail
})
const mapDispatchToProps = dispatch => ({
    getPoDetail: (id) => dispatch(getPoDetail(id))
})


export default connect(mapStateToProps, mapDispatchToProps)(TransactionDetailScreens)

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 15
    },
    promo: {
        borderWidth: 0.5,
        borderRadius: 5,
        margin: 10,
        padding: 10
    },
    header: {
        padding: 15,
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        elevation: 5,
        margin: 5,
        marginTop: 2
    },
    subHeader: {
        marginLeft: 15,
        flex: 1
    },
})