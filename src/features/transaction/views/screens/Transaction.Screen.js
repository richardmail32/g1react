import React, { useCallback, useState } from 'react'
import { StyleSheet, Dimensions, View, ScrollView, Image, RefreshControl, Touchable, TouchableOpacity } from 'react-native'
import Swiper from 'react-native-swiper'
import { connect } from 'react-redux';
import { getPo, selectCurrentTransaction } from '../../redux'
import { createStructuredSelector } from 'reselect'
import { useTheme, ActivityIndicator, Avatar, Title, Card, Text } from 'react-native-paper';
import { useFocusEffect } from '@react-navigation/native';
import { FlatGrid } from 'react-native-super-grid';
import { useNavigation } from '@react-navigation/native';

function TransactionScreen({ currentTransaction, getPo }) {
    const navigation = useNavigation();
    const [visible, setVisible] = useState(false);
    const theme = useTheme();
    console.log('currentTransaction:', currentTransaction);
    useFocusEffect(
        useCallback(() => {
            getPo();
            return () => setVisible(false);
        }, [],
        ))

    const refreshComponent = (
        <RefreshControl
            colors={[theme.colors.primary]}
            refreshing={false}
            onRefresh={getPo}
        />
    )


    if (!currentTransaction) {
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
                refreshControl={refreshComponent}>
                <ActivityIndicator size="small" />
            </ScrollView>
        )
    }
    return (
        <ScrollView >
            <View>
                {currentTransaction?.map((po) => (
                    <Card style={{
                        padding: 20,
                        margin: 10,
                        borderRadius: 10,
                    }}>
                        <TouchableOpacity onPress={() => navigation.navigate('Transaction.TransactionDetailScreen', { po: po.id })}>
                            <Text style={{ fontWeight: '700' }}>
                                No. Pesanan: {po.kode_po}
                            </Text>
                            <Text>
                                Tanggal: {po.tanggal}
                            </Text>
                            <View style={styles.text}>
                                {po.status_po === 'Menunggu' && <Text style={{ fontWeight: '700', paddingTop: 10, color: '#FFAFAF' }}>
                                    {po.status_po}
                                </Text>}
                                {po.status_po === 'Dalam Pengiriman' && <Text style={{ fontWeight: '700', paddingTop: 10, color: '#EEEEEE' }}>
                                    {po.status_po}
                                </Text>}
                                {po.status_po === 'Diterima' && <Text style={{ fontWeight: '700', paddingTop: 10, color: '#A2D39C' }}>
                                    {po.status_po}
                                </Text>}
                                {po.status_po === 'Selesai' && <Text style={{ fontWeight: '700', paddingTop: 10, color: 'orange' }}>
                                    {po.status_po}
                                </Text>}
                                {po.status_po === 'Dibatalkan' && <Text style={{ fontWeight: '700', paddingTop: 10, color: 'red' }}>
                                    {po.status_po}
                                </Text>}
                                <Text style={{ paddingTop: 10, fontWeight: '700', color: 'red' }}>
                                    Rp {po.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </Card>
                ))}
            </View>
            {/* <FlatGrid
                itemDimension={130}
                data={currentTransaction}
                style={styles.gridView}
                // staticDimension={300}
                // fixed
                // spacing={1}
                renderItem={({ item }) => (
                    <Card>
                        <View style={[styles.itemContainer]}>

                            <Text style={styles.itemName}>{item.id}</Text>

                        </View>
                    </Card>
                )}
            /> */}
        </ScrollView>
    );
}

const mapStateToProps = createStructuredSelector({
    currentTransaction: selectCurrentTransaction
})

const mapDispatchToProps = dispatch => ({
    getPo: () => dispatch(getPo())
})

export default connect(mapStateToProps, mapDispatchToProps)(TransactionScreen);

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    gridView: {
        marginTop: 10,
        flex: 1,
    },
    itemContainer: {
        justifyContent: 'flex-end',
        borderRadius: 5,
        padding: 10,
        height: 150,
    },
    itemName: {
        fontSize: 16,
        color: '#fff',
        fontWeight: '600',
    },
    itemCode: {
        fontWeight: '600',
        fontSize: 12,
        color: '#fff',
    },
})