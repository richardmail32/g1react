import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

function DiscountScreen () {
    return (
        <View style={styles.screen}>
            <Text>Discount Screen</Text>
        </View>
    );
}

export default DiscountScreen;

const styles = StyleSheet.create({
    screen:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})