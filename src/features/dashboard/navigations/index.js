import { DashboardScreen, ProdukScreen } from '../views';
import CartScreen from '../views/screens/Cart.Screen';
import ProdukDetailScreen from '../views/screens/ProdukDetail.Screen';


export default {
    'Dashboard.DashboardScreen': {
        screen: DashboardScreen,
        options: {
            headerShown: false,
            contentStyle: {
                backgroundColor: '#ffffff'
            },
        },
    },
    'Produk.ProdukScreen': {
        screen: ProdukScreen,
        options: {
            title: 'Produk',
            headerShown: true,
            contentStyle: {
                backgroundColor: '#ffffff'
            },
        },
    },

    'Produk.ProdukDetailScreen': {
        screen: ProdukDetailScreen,
        options: {
            title: 'Produk Detail',
            headerShown: true,
            contentStyle: {
                backgroundColor: '#ffffff'
            },
        },
    },

    'Cart.CartScreen': {
        screen: CartScreen,
        options: {
            title: 'Cart',
            headerShown: true,
            contentStyle: {
                backgroundColor: '#ffffff'
            },
        },
    },

}