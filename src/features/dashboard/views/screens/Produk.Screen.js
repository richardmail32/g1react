import { View, StyleSheet, ScrollView, RefreshControl, SafeAreaView, Image, TouchableOpacity, Dimensions } from 'react-native'
import React, { useCallback, useState } from 'react'
import { getKategori, getPemasok, getProduk, selectCurrentKategori, selectCurrentPemasok, selectCurrentProduk } from '../../redux'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux';
import { useFocusEffect } from '@react-navigation/native';
import { useTheme, ActivityIndicator, Avatar, Title, Text, Card } from 'react-native-paper';
import { Picker } from '@react-native-picker/picker';
import images from '../../../../assets';
import { useNavigation } from '@react-navigation/native';

const cardGap = 10;
const cardWidth = (Dimensions.get('window').width - cardGap * 3) / 2;

function ProdukScreen({ currentPemasok, getPemasok, currentProduk, getProduk, currentKategori, getKategori }) {
    const [visible, setVisible] = useState(false);
    const navigation = useNavigation();
    const [selectedValue, setSelectedValue] = useState('');
    console.log(selectedValue);
    const theme = useTheme()
    // console.log('currentPemasok:', currentPemasok);
    useFocusEffect(
        useCallback(() => {
            getPemasok();
            return () => setVisible(false);
        }, [],
        ))

    console.log('currentKategori:', currentKategori);
    useFocusEffect(
        useCallback(() => {
            getKategori(currentPemasok?.id);
            return () => setVisible(false);
        }, [currentPemasok?.id],
        ))

    console.log('currentProduk:', currentProduk);
    useFocusEffect(
        useCallback(() => {
            getProduk(currentPemasok?.id, selectedValue);
            return () => setVisible(false);
        }, [currentPemasok?.id, selectedValue],
        ))

    const refreshComponent = (
        <RefreshControl
            colors={[theme.colors.primary]}
            refreshing={false}
            onRefresh={getPemasok}
        />
    )

    if (!currentPemasok) {
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
                refreshControl={refreshComponent}>
                <ActivityIndicator size="small" />
            </ScrollView>
        )
    }
    else if (!currentProduk) {
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
                refreshControl={refreshComponent}>
                <ActivityIndicator size="small" />
            </ScrollView>
        )
    }
    return (
        <SafeAreaView style={styles.container}>
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1 }}
                refreshControl={refreshComponent}>
                <View style={styles.header}>
                    <Image
                        style={{
                            height: 60,
                            width: 60,
                        }}
                        source={images.logo}
                    />
                    <View style={styles.subHeader}>
                        <Title style={{ fontSize: 16 }}>
                            {currentPemasok.nama_perusahaan}
                        </Title>
                        <Text style={{ flexWrap: 'wrap' }}>{currentPemasok.alamat}</Text>
                    </View>
                </View>
                <View style={{
                    borderWidth: 0.5,
                    borderRadius: 5,
                    margin: 10
                }}>
                    <Picker
                        selectedValue={selectedValue}
                        style={{
                            height: 50,
                            idth: '100%'
                        }}
                        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                    >
                        <Picker.Item label='Semua' value='' />
                        {currentKategori?.map((data) => (
                            <Picker.Item label={data.nama} value={data.id.toString()} />
                        ))}
                    </Picker>
                </View>

                <View style={styles.mainContainer}>
                    {currentProduk.map((result, i) => (
                        <Card
                            onPress={() => navigation.navigate('Produk.ProdukDetailScreen', { result: result })}
                            key={i}
                            style={{
                                marginBottom: cardGap,
                                marginLeft: i % 2 !== 0 ? cardGap : 0,
                                width: cardWidth,
                                ...styles.card
                            }}
                        >
                            <Image
                                source={{ uri: result?.produk.foto_1 }}
                                style={{ width: '100%', height: 150 }}
                            />
                            <View style={{ margin: 10 }}>
                                <Text style={{
                                    fontSize: 14,
                                }}>
                                    {result.produk.nama}
                                </Text>
                                <Text style={{
                                    fontSize: 14,
                                    fontWeight: '700'
                                }}>
                                    Rp {Number.parseInt(result.produk.harga).toLocaleString('id-ID')}
                                </Text>

                                <Text style={{
                                    fontSize: 14
                                }}>
                                    Stok: {result.produk.jumlah_stok}
                                </Text>
                            </View>
                        </Card>

                    ))}
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const mapStateToProps = createStructuredSelector({
    currentPemasok: selectCurrentPemasok,
    currentKategori: selectCurrentKategori,
    currentProduk: selectCurrentProduk
})

const mapDispatchToProps = dispatch => ({
    getPemasok: () => dispatch(getPemasok()),
    getKategori: (pemasokid) => dispatch(getKategori(pemasokid)),
    getProduk: (pemasokid, selectedValue) => dispatch(getProduk(pemasokid, selectedValue))
})
export default connect(mapStateToProps, mapDispatchToProps)(ProdukScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f4f4f6'
    },
    mainContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    card: {
        backgroundColor: 'white',
        borderRadius: 16,
        shadowOpacity: 0.2,
    },
    header: {
        padding: 15,
        paddingBottom: 0,
        flexDirection: 'row',
        margin: 5,
        marginTop: 2
    },
    subHeader: {
        marginLeft: 15,
        flex: 1
    },
})