import { Alert, Dimensions, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useCallback, useEffect, useState } from 'react'
import Swiper from 'react-native-swiper';
import { Button, Card } from 'react-native-paper';
import InputSpinner from "react-native-input-spinner";
import { Dashboard } from '../../../../services';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { Toast } from '../../../../configs';
import { getCart, selectCurrentCart } from '../../redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import NumberFormat from 'react-number-format';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const cardGap = 10;
const cardWidth = (Dimensions.get('window').width - cardGap * 3) / 2;

function ProdukDetailScreen({ route, getCart, currentCart }) {
    const { result } = route.params;
    const navigation = useNavigation();
    const [visible, setVisible] = useState(false);
    var [number, setNumber] = useState(0);
    var qtyCart = 0;

    console.log('currentCart:', currentCart);
    useFocusEffect(
        useCallback(() => {
            getCart();
            return () => setVisible(false);
        }, [],
        ))

    const qty = currentCart.barang?.map(produk => {
        if (produk.id_katalog_harga === result.produk.id_katalog) {
            qtyCart = produk.jumlah;
        }
    })

    const submit = async () => {
        try {
            const reqBody = {
                id_katalog_harga: result.produk.id_katalog,
                jumlah: number + qtyCart
            };

            let formData = new FormData();

            for (const key in reqBody) {
                formData.append(key, reqBody[key]);
            }

            const data = await Dashboard.postCart(formData);
            console.log("result", data.success);

            if (data.success === true) {
                navigation.goBack(Toast('success',
                'Berhasil',
                data.message));
            } else {
                Toast('error',
                    'Gagal',
                    data.message);
            }
        } catch (error) {
            if (error.response && error.response.status === 400) {
                Toast('error')
            }
            console.log(error);
        }
    }
    console.log(route);
    return (
        <SafeAreaView style={styles.container}>
            <View style={{
                flex: .4,
                justifyContent: 'flex-start',
                paddingHorizontal: 5,
                marginTop: -20
            }}>
                <Swiper
                    autoplay
                    autoplayTimeout={5}
                    dotStyle={styles.swiperDot}
                    activeDotStyle={styles.swiperActiveDot}
                    paginationStyle={styles.swiperPagination}
                >
                    <Image
                        style={{
                            flex: 1,
                            height: '100%',
                            width: '100%',
                            borderRadius: 10,
                        }}
                        resizeMode='center'
                        source={{ uri: result.produk.foto_1 }}
                    />
                    <Image
                        style={{
                            flex: 1,
                            height: '100%',
                            width: '100%',
                            borderRadius: 10,
                        }}
                        resizeMode='center'
                        source={{ uri: result.produk.foto_2 }}
                    />
                </Swiper>
            </View>
            <View style={styles.text}>
                <Text style={{ fontWeight: '700', fontSize: 16 }}>
                    {result.produk.nama}
                </Text>

                <Text style={{ fontSize: 16 }}>
                    Stok: {result.produk.jumlah_stok}
                </Text>
            </View>
            <View
                style={styles.mainContainer}
            >
                {result.varian.map((varian, i) => (
                    <Card key={i}
                        style={{
                            marginBottom: cardGap,
                            marginLeft: i % 2 !== 0 ? cardGap : 0,
                            width: cardWidth,
                            ...styles.card
                        }}>
                        <View style={{ alignItems: 'center' }}>
                            <Text>
                                {varian.nama_varian}: <Text>Rp {Number.parseInt(varian.value * result.produk.harga).toLocaleString('id-ID')}</Text>
                            </Text>
                        </View>
                        <View>
                            <InputSpinner
                                color={"#40c5f4"}
                                onIncrease={(num) => {
                                    console.log(num + number);
                                    setNumber(number + varian.value);
                                }}
                                onDecrease={(num) => {
                                    setNumber(number - varian.value);
                                }}
                            />
                        </View>
                    </Card>
                ))}
            </View>
            <Button
                mode="contained"
                onPress={submit}
                style={styles.button}>
                <View style={{ width: 16, height: 1 }} />
                <Text style={{ color: '#ffffff', textTransform: 'capitalize' }}>
                    {qtyCart + number}
                </Text>
            </Button>
        </SafeAreaView>
    )
}

const mapStateToProps = createStructuredSelector({
    currentCart: selectCurrentCart,
})

const mapDispatchToProps = dispatch => ({
    getCart: () => dispatch(getCart())
})
export default connect(mapStateToProps, mapDispatchToProps)(ProdukDetailScreen);

const styles = StyleSheet.create({
    container: { flex: 1 },
    text: {
        // flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 15
    },
    wrap: {
        width: width,
        height: 140,
        borderRadius: 10,
    },
    swiperDot: {
        backgroundColor: '#DDDDDD',
        width: 8,
        height: 8,
        borderRadius: 4
    },
    swiperActiveDot: {
        backgroundColor: 'red',
        width: 8,
        height: 8,
        borderRadius: 4
    },
    swiperPagination: {
        position: 'absolute',
        left: 0,
        bottom: 5,
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    circle: {
        backgroundColor: 'red',
        position: 'absolute',
        height: 80,
        width: Dimensions.get('window').width + 80,
        top: 0,
        left: -40,
        borderBottomLeftRadius: 80,
        borderBottomRightRadius: 80
    },
    mainContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    card: {
        backgroundColor: 'white',
        borderRadius: 16,
        shadowOpacity: 0.2,
        padding: 20
    },
})