/* eslint-disable prettier/prettier */
import React, { useCallback, useState } from 'react'
import { StyleSheet, Dimensions, View, ScrollView, Image, RefreshControl, SafeAreaView, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux';
import { getBanner, selectCurrentDashboard } from '../../redux'
import { createStructuredSelector } from 'reselect'
import { useTheme, ActivityIndicator, Avatar, Title, Text, Card } from 'react-native-paper';
import { useFocusEffect } from '@react-navigation/native';
import Images from '../../../../assets';
import Swiper from 'react-native-swiper';
import { ImageSlider } from "react-native-image-slider-banner";
import { useNavigation } from '@react-navigation/native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

function DashboardScreen({ currentDashboard, getBanner }) {
    const [visible, setVisible] = useState(false);
    const navigation = useNavigation();
    const theme = useTheme();
    console.log('currentDashboard:', currentDashboard);

    useFocusEffect(
        useCallback(() => {
            getBanner();
            return () => setVisible(false);
        }, [],
        ))

    const refreshComponent = (
        <RefreshControl
            colors={[theme.colors.primary]}
            refreshing={false}
            onRefresh={getBanner}
        />
    )

    if (!currentDashboard) {
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
                refreshControl={refreshComponent}>
                <ActivityIndicator size="small" />
            </ScrollView>
        )
    }


    return (

        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ flexGrow: 1 }} refreshControl={refreshComponent}>
            <View style={styles.circle}>

            </View>
            {/* <View style={{
                flex: .4, justifyContent: 'flex-start', paddingHorizontal: 5, marginTop: -20
            }}>
                <Swiper
                    autoplay
                    autoplayTimeout={5}
                    dotStyle={styles.swiperDot}
                    activeDotStyle={styles.swiperActiveDot}
                    paginationStyle={styles.swiperPagination}
                >
                    {currentDashboard.map((banner, i) => (
                        <Image
                            key={i}
                            style={{
                                flex: 1,
                                height: '100%',
                                width: '100%',
                                borderRadius: 20,
                            }}
                            resizeMode='center'
                            source={{ uri: banner.img_url }}
                        />

                    ))}
                </Swiper>

            </View> */}

            <View >
                <ImageSlider
                    data={
                        currentDashboard.map((banner) => (
                            { img: banner.img_url }
                        ))}
                    caroselImageStyle={{ height: 176, }}
                    previewImageStyle={{ height: 170 }}
                    indicatorContainerStyle={{ top: 40 }}
                    autoPlay={false}
                    closeIconColor="#fff"
                />
            </View>


            <View>
                <Title style={{ marginHorizontal: 15, marginTop: 15, marginBottom: -5, fontSize: 18 }}>
                    Belanja
                </Title>
                <View style={{ padding: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Card style={{ padding: 10, borderRadius: 10 }}>
                        <TouchableOpacity onPress={() => navigation.navigate('Produk.ProdukScreen')}>
                            <View style={{ alignItems: 'center', }}>
                                <Image style={{ width: 65, height: 65 }}
                                    source={Images.order}
                                />
                                <Text style={{ fontWeight: '600', marginTop: 5 }}>
                                    Pesan Barang
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </Card>
                </View>
            </View>
        </ScrollView>
    );
}

const mapStateToProps = createStructuredSelector({
    currentDashboard: selectCurrentDashboard
})

const mapDispatchToProps = dispatch => ({
    getBanner: () => dispatch(getBanner())
})
export default connect(mapStateToProps, mapDispatchToProps)(DashboardScreen);

const styles = StyleSheet.create({
    container: { flex: 1 },
    wrap: {
        width: width,
        height: 140,
        borderRadius: 10,
    },
    swiperDot: {
        backgroundColor: '#DDDDDD',
        width: 8,
        height: 8,
        borderRadius: 4
    },
    swiperActiveDot: {
        backgroundColor: 'red',
        width: 8,
        height: 8,
        borderRadius: 4
    },
    swiperPagination: {
        position: 'absolute',
        left: 0,
        bottom: 10,
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    circle: {
        backgroundColor: 'red',
        position: 'absolute',
        height: 80,
        width: Dimensions.get('window').width + 80,
        top: 0,
        left: -40,
        borderBottomLeftRadius: 80,
        borderBottomRightRadius: 80
    }
});