import { ActivityIndicator, Image, ScrollView, StyleSheet, View } from 'react-native'
import React, { useCallback, useState } from 'react'
import { useFocusEffect } from '@react-navigation/native';
import { Card, useTheme, Text, Title } from 'react-native-paper';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getCart, selectCurrentCart } from '../../redux';
import InputSpinner from "react-native-input-spinner";
import { Dashboard } from '../../../../services';
import { Toast } from '../../../../configs';

function CartScreen({ currentCart, getCart }) {
    const [visible, setVisible] = useState(false);
    const theme = useTheme();
    console.log('currentCart:', currentCart);
    const [loading, setLoading] = useState(false);
    var [number, setNumber] = useState(0);
    const [status, setStatus] = useState(false);


    useFocusEffect(
        useCallback(() => {
            getCart();
            return () => setVisible(false);
        }, [],
        ))

    const onCart = (idkatalog, total) => async () => {
        setLoading(true);
        try {
            const reqBody = {
                id_katalog_harga: idkatalog,
                jumlah: total
            };

            console.log('total', total + 1);

            console.log('1', reqBody);

            let formData = new FormData();

            for (const key in reqBody) {
                formData.append(key, reqBody[key]);
            }

            const data = await Dashboard.postCart(formData);

            if (data.success === true) {
                getCart();
                Toast('success', 'Berhasil', data.message); 
                setStatus(false);
            } else {
                Toast('error', 'Gagal', data.message);
                setStatus(true);
            }


        } catch (error) {
            if (error.response && error.response.status === 400) {
                Toast('error')
            }
            console.log(error);
        }
    }

    if (!onCart) {
       
    }
    return (
        <ScrollView>
            <View>
                <Title style={{ fontSize: 15, marginLeft: 15, marginTop: 15 }}>
                    Alamat Pengiriman
                </Title>
                <Card style={{
                    padding: 20,
                    margin: 10,
                    borderRadius: 10,
                }}>
                    <Text style={{ fontWeight: '700' }}>
                        {currentCart.pemasok?.nama_perusahaan} ({currentCart.pemasok?.msisdn})
                    </Text>
                    <Text>
                        {currentCart.pemasok?.alamat}
                    </Text>
                </Card>

            </View>
            <View>
                <Title style={{ fontSize: 15, marginLeft: 15, marginTop: 15 }}>
                    Produk
                </Title>
                {currentCart.barang?.map((barang, i) => (
                    <Card style={styles.header}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image
                                key={i}
                                style={{
                                    height: 60,
                                    width: 60,
                                }}
                                source={{ uri: barang.foto_1 }}
                            />
                            <View style={styles.subHeader}>
                                <Text style={{ fontWeight: '700', fontSize: 16 }}>
                                    {barang.nama_produk}
                                </Text>
                                <Text>
                                    {barang.jumlah}x Rp {Number.parseInt(barang.harga).toLocaleString('id-ID')}
                                </Text>
                            </View>
                        </View>
                        <View style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', }}>
                            <Text style={{ fontWeight: '700' }}>
                                Rp. {Number.parseInt(barang.harga_setelah_diskon * barang.jumlah).toLocaleString('id-ID')}
                            </Text>
                            <InputSpinner
                                value={barang.jumlah}
                                height={30}
                                fontSize={9}
                                skin="round"
                                width={100}
                                color={"#40c5f4"}
                                buttonRightDisabled = {status}
                                onIncrease={
                                    onCart(barang.id_katalog_harga, barang.jumlah + 1)
                                }
                                onDecrease={
                                    onCart(barang.id_katalog_harga, barang.jumlah - 1)
                                }
                                onChange={(num) => {
                                    setNumber(num);
                                }}
                            />
                        </View>
                    </Card>
                ))}

            </View>
        </ScrollView>
    )
}

const mapStateToProps = createStructuredSelector({
    currentCart: selectCurrentCart
})

const mapDispatchToProps = dispatch => ({
    getCart: () => dispatch(getCart())
})

export default connect(mapStateToProps, mapDispatchToProps)(CartScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 15
    },
    header: {
        padding: 15,
        backgroundColor: '#ffffff',
        elevation: 5,
        margin: 5,
        marginTop: 2
    },
    subHeader: {
        marginLeft: 15,
        flex: 1
    }
})
