import { dashboardTypes } from './'

const initState = {
    currentDashboard: null,
    currentPemasok: null,
    currentKategori: null,
    currentProduk: [],
    currentCart:[]
}

export const dashboardReducers = (state = initState, action) => {
    switch (action.type) {
        case dashboardTypes.SET_DASHBOARD:
            return { ...state, ...action.payload };
        case dashboardTypes.SET_DEFAULT_DASHBOARD:
            return initState;
        default:
            return state;
    }
}