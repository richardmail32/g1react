import { dashboardTypes } from ".";
import { Dashboard } from '../../../services'

export const setDashboard = (payload) => ({
    type: dashboardTypes.SET_DASHBOARD,
    payload,
});

export const setDefaultDashboard = () => ({
    type: dashboardTypes.SET_DEFAULT_DASHBOARD,
});

export const setPemasok = (payload) => ({
    type: dashboardTypes.SET_DASHBOARD,
    payload,
});

export const setDefaultPemasok = () => ({
    type: dashboardTypes.SET_DEFAULT_DASHBOARD,
});

export const setKategori = (payload) => ({
    type: dashboardTypes.SET_DASHBOARD,
    payload,
});

export const setDefaultKategori = () => ({
    type: dashboardTypes.SET_DEFAULT_DASHBOARD,
});

export const setProduk = (payload) => ({
    type: dashboardTypes.SET_DASHBOARD,
    payload,
});

export const setDefaultProduk = () => ({
    type: dashboardTypes.SET_DEFAULT_DASHBOARD,
});

export const setCart = (payload) => ({
    type: dashboardTypes.SET_DASHBOARD,
    payload,
});

export const setDefaultCart = () => ({
    type: dashboardTypes.SET_DEFAULT_DASHBOARD,
});


export const getBanner = () => async dispatch => {
    try {
        const { data } = await Dashboard.getBanner();
        dispatch(
            setDashboard({
                currentDashboard: data
            })
        )
        // console.log('setDashboard', data);
    } catch (error) {
        // console.log('setDashboard', error);
    }
}

export const getPemasok = () => async dispatch => {
    try {
        const { data } = await Dashboard.getPemasok();
        dispatch(
            setPemasok({
                currentPemasok: data[0]
            })
        )
        // console.log('setPemasok', data);
    } catch (error) {
        // console.log('setPemasok', error);
    }
}

export const getKategori = (pemasokid) => async dispatch => {
    try {
        const { data } = await Dashboard.getKategori(pemasokid);
        dispatch(
            setKategori({
                currentKategori: data
            })
        )
        console.log('currentKategori', data);
    } catch (error) {
        console.log('setProduk', error);
    }
}

export const getProduk = (pemasokid, selectedValue) => async dispatch => {
    try {
        const { data } = await Dashboard.getProduk(pemasokid, selectedValue);
        dispatch(
            setProduk({
                currentProduk: data
            })
        )
        console.log('currentProduk', data);
        console.log(selectedValue);
    } catch (error) {
        // console.log('setProduk', error);
    }
}

export const getCart = () => async dispatch => {
    try {
        const { data } = await Dashboard.getCart();
        dispatch(
            setCart({
                currentCart: data
            })
        )
        console.log('currentProduk', data);
    } catch (error) {
        // console.log('setProduk', error);
    }
}