import { createSelector } from 'reselect'

const selectDashboard = (state) => state.dashboard;
const selectPemasok = (state) => state.dashboard;
const selectKategori = (state) => state.dashboard;
const selectProduk = (state) => state.dashboard;
const selectCart = (state) => state.dashboard;

export const selectCurrentDashboard = createSelector(
    [selectDashboard],
    dashboard => dashboard.currentDashboard
);

export const selectCurrentPemasok = createSelector(
    [selectPemasok],
    dashboard => dashboard.currentPemasok
);

export const selectCurrentKategori = createSelector(
    [selectKategori],
    dashboard => dashboard.currentKategori
);

export const selectCurrentProduk = createSelector(
    [selectProduk],
    dashboard => dashboard.currentProduk
);

export const selectCurrentCart = createSelector(
    [selectCart],
    dashboard => dashboard.currentCart
);