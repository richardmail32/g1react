import React, { Component, useEffect } from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Toast from 'react-native-toast-message';
import configStore from '../state_management'

const { store } = configStore();

import {
    NavigationContainer,
    DefaultTheme as NavigationDefaultTheme,
} from '@react-navigation/native';

import {
    DefaultTheme as PaperDefaultTheme,
    Provider as PaperProvider,
} from 'react-native-paper';


const CombinedDefaultTheme = {
    ...PaperDefaultTheme,
    ...NavigationDefaultTheme,
    colors: {
        ...PaperDefaultTheme.colors,
        ...NavigationDefaultTheme.colors,
        primary: '#b71c1c',
    },
};


import Authentication from '../features/authentication/navigations';
import User from "../features/user/navigations"
import MainMenu from "../features/main_menu";
import Dashboard from "../features/dashboard/navigations";
import Transaction from "../features/transaction/navigations";

const Stack = createNativeStackNavigator();

const getInitialRoute = () => {
    const { authentication } = store.getState();
    return authentication.userToken ? 'MainMenuScreen' : 'Authentication.LoginScreen';
};

function RootNavigation() {

    return (
        <PaperProvider theme={CombinedDefaultTheme}>
            <NavigationContainer theme={CombinedDefaultTheme}>
                <Stack.Navigator
                    initialRouteName={getInitialRoute()}
                    screenOptions={{
                        animation: 'slide_from_right',
                        orientation: 'portrait'
                    }}>
                    {Object.entries({
                        ...Authentication,
                        ...User,
                        ...MainMenu,
                        ...Dashboard,
                        ...Transaction
                    }).map(([name, component], id) => (
                        <Stack.Screen
                            key={id}
                            name={name}
                            component={component.screen}
                            options={component.options}
                        />
                    ))}
                </Stack.Navigator>
            </NavigationContainer>
            <Toast />
        </PaperProvider>
    );
}

export default RootNavigation;