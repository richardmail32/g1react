import { FetchInterceptor } from "../../configs";

class Dashboard {
    static async getBanner() {
        const response = await FetchInterceptor.get('/api/banner');
        return response;
    }
    static async getPemasok() {
        const response = await FetchInterceptor.get('/api/pemasok?pemasok=');
        return response;
    }
    static async getKategori(pemasokid) {
        const response = await FetchInterceptor.get('/api/kategori-produk/' + pemasokid);
        return response;
    }
    static async getProduk(pemasokid, selectedValue) {
        console.log(pemasokid);
        const response = await FetchInterceptor.get('/api/produk/' + pemasokid + '?id_kategori=' + selectedValue);
        return response;
    }
    static async postCart(reqBody) {
        const response = await FetchInterceptor.post('/api/keranjang', reqBody);
        return response;
    }
    static async getCart() {
        const response = await FetchInterceptor.get('/api/keranjang');
        return response;
    }

}

export default Dashboard;
