import { FetchInterceptor } from "../../configs";

class Transaction {
    static async getPo() {
        const response = await FetchInterceptor.get('/api/po');
        return response;
    }

    static async getPoDetail(id) {
        const response = await FetchInterceptor.get('/api/po/' + id);
        return response;
    }
}

export default Transaction;