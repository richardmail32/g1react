import { FetchInterceptor } from "../../configs";

class User {
    static async getUser() {
        const response = await FetchInterceptor.get('/api/profile');
        return response;
    }
}

export default User;