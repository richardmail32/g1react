import { FetchInterceptor } from "../../configs";

class Authentication {
    static async login(reqBody) {
        const response = await FetchInterceptor.post('api/login/pembeli', reqBody);
        return response;
    }

    static async register(reqBody) {
        const response = await FetchInterceptor.post('/api/v2/pembeli', reqBody);
        return response;
    }

    static async getStockist() {
        const response = await FetchInterceptor.get('/api/stockist');
        return response;
    }

}

export default Authentication;
