import { combineReducers } from "redux";
import { authenticationReducers } from "../features/authentication/redux";
import { dashboardReducers } from "../features/dashboard/redux";
import { transactionReducers } from "../features/transaction/redux";
import { userReducers } from "../features/user/redux";


const rootReducer = combineReducers({
    authentication: authenticationReducers,
    user: userReducers,
    dashboard: dashboardReducers,
    transaction: transactionReducers,
});

export default rootReducer;