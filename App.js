import React from "react";
import { View, StatusBar } from "react-native";
import { RootNavigation } from "./src/configs";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from "react-redux";
import configStore from "./src/state_management"

const { store, persistor } = configStore();

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <View style={{ flex: 1 }}>
          <StatusBar backgroundColor={'#FFFFFF'} barStyle="dark-content" />
          <RootNavigation />
        </View>
      </PersistGate>
    </Provider>
  );
}

export default App;